package mergeapi

import (
	"gitlab.com/mergetb/engine/api/portal/models"
)

// XDCs lists the XDC for the given experiment.
func (ma *MergeAPI) XDCs(project, experiment string) ([]models.XdcInfo, error) {

	var xdcs []models.XdcInfo
	err := ma.GetObj("projects/"+project+"/experiments/"+experiment+"/xdc", nil, &xdcs)
	if err != nil {
		return nil, err
	}

	return xdcs, nil
}

// Spawn  ...
func (ma *MergeAPI) Spawn(project, experiment, xdcid string) error {

	return ma.Put("projects/"+project+"/experiments/"+experiment+"/xdc/"+xdcid, nil)
}

// DeleteXDC ...
func (ma *MergeAPI) DeleteXDC(project, experiment, xdcid string) error {

	return ma.Del("projects/"+project+"/experiments/"+experiment+"/xdc/"+xdcid, nil)
}

// XDCToken ...
func (ma *MergeAPI) XDCToken(project, experiment, xdcid string) (string, error) {

	var token string
	err := ma.Get(
		"projects/"+project+"/experiments/"+experiment+"/xdc/"+xdcid+"/token",
		&token,
	)
	if err != nil {
		return "", err
	}

	return token, nil
}

// Attach - create a network connection between an xdc and a materialization.
func (ma *MergeAPI) Attach(project, experiment, realization, xdcid string) error {

	return ma.Put("projects/"+project+"/experiments/"+experiment+"/xdc/"+xdcid+"/connection/"+realization, nil)
}

// Detach - teardown the network connection between an XDC and a materialization.
func (ma *MergeAPI) Detach(project, experiment, realization, xdcid string) error {

	return ma.Del("projects/"+project+"/experiments/"+experiment+"/xdc/"+xdcid+"/connection/"+realization, nil)
}
