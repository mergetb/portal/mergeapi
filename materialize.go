package mergeapi

import (
	"gitlab.com/mergetb/engine/api/portal/models"
)

// Status return the current status of the given materialization.
func (ma *MergeAPI) Status(project, experiment, realization string) (*models.MaterializationStatus, error) {

	status := &models.MaterializationStatus{}

	err := ma.GetObj(
		"/projects/"+project+"/experiments/"+experiment+"/realizations/"+realization+"/materialization/status",
		nil,
		&status,
	)
	if err != nil {
		return nil, err
	}

	return status, nil
}

// Materialization gets info about an active materialization
func (ma *MergeAPI) Materialization(project, experiment, realization string) (*models.Materialization, error) {

	m := &models.Materialization{}
	err := ma.GetObj(
		"/projects/"+project+"/experiments/"+experiment+"/realizations/"+realization+"/materialization",
		nil,
		&m,
	)
	if err != nil {
		return nil, err
	}

	return m, nil
}

// Materialize materializes an realization. (Configures all reserved test nodes and "activates" the experiment.)
func (ma *MergeAPI) Materialize(project, experiment, realizeid string) error {

	return ma.Put(
		"/projects/"+project+"/experiments/"+experiment+"/realizations/"+realizeid+"/materialization",
		nil,
	)
}

// DeleteMaterialization ...
func (ma *MergeAPI) DeleteMaterialization(project, experiment, realizeid string) error {

	return ma.Del(
		"/projects/"+project+"/experiments/"+experiment+"/realizations/"+realizeid+"/materialization",
		nil,
	)
}

func (ma *MergeAPI) Materializations() ([]*models.MzInfo, error) {

	var mtzs []*models.MzInfo
	err := ma.GetObj("materializations", nil, &mtzs)
	if err != nil {
		return nil, err
	}

	return mtzs, nil
}

// WireguardAttach - create a network connection between a non-XDC workstation/machine/container and a materialization.
// Note that the key requiered is a Wireguard public key. This key will be added to the list of allowed keys that can
// access the materialization. The returned key must then be added to the client side of the existing Wireguard connection.)
func (ma *MergeAPI) WireguardAttach(project, experiment, realizeid, key string) (*models.MaterializeAttachResponse, error) {

	resp := &models.MaterializeAttachResponse{}
	err := ma.PutObj(
		"/projects/"+project+"/experiments/"+experiment+"/realizations/"+realizeid+"/materialization/attach",
		&models.AddPublicKey{Key: key},
		&resp,
	)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// WireguardDetach - tear down the wireguard connection to the given materialization.
func (ma *MergeAPI) WireguardDetach(project, experiment, realizeid string) error {
	return ma.Del(
		"/projects/"+project+"/experiments/"+experiment+"/realizations/"+realizeid+"/materialization/attach",
		nil,
	)
}
