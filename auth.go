package mergeapi

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"strconv"
)

var (
	oas = NewOAuthServer()
	ids = NewIdentityServer()
)

// Error implements the error interface for AuthError
func (ae *AuthError) Error() string {
	if ae.Err != nil {
		return fmt.Sprintf("%s: %+v", ae.Msg, ae.Err)
	}
	return fmt.Sprintf("%s", ae.Msg)
}

// Use OAuth for authentication.
func (ma *MergeAPI) SetAuthProvider(cliID, ep string) {
	ma.AuthMode = OAuth
	oas.SetAuthProvider(cliID, ep)
}

//
// SetIdentityServer - This will cause Merge API to be
// authenticated via an Identity Server (IS). The IS has a single
// '/login' endpoint that takes a username & password and returns
// a session token. This is not OAuth. If the IS is set, OAuth
// will not be used.
//
func (ma *MergeAPI) SetIdentityServer(server string) {
	ma.AuthMode = IdentServer
	ids.SetServer(server)
}

// Login to merge API and store an auth token locally.
func (ma *MergeAPI) Login(passwd string) error {

	var tk AuthToken
	var err error

	if ma.AuthMode == IdentServer {
		tk, err = ids.Login(ma.APIUser, passwd, ma)
	} else {
		tk, err = oas.Login(ma.APIUser, passwd)
	}
	if err != nil {
		return err
	}

	// Write all data to disk to be read later.
	err = setActiveUser(ma.APIUser)
	if err != nil {
		return &AuthError{"failed to save active user", err}
	}

	// Write out data to files.
	// This should really just be a config file instead of a bunch of separate files.
	files := [][2]string{
		{"token", tk.AccessToken},
		{"reftoken", tk.RefreshToken},
		{"id", ma.APIUser},
		{"app", ma.AppName},
		{"api", ma.URL},
		{"certpath", ma.Certpath},
		{"authmode", ma.AuthMode.String()},
	}

	for _, f := range files {

		err = ma.writeFile(f[1], f[0])
		if err != nil {
			return &AuthError{"failed to save " + f[0], err}
		}

	}

	return nil
}

// Logout of an existing merge auth session.
func (ma *MergeAPI) Logout() error {

	// GTL Auth0 should be made aware that someone is logging out, but
	// we have the concept of application/OS user/User ID which auth0
	// knows nothing about. Logging out "tommy" would log tommy out
	// of all applications using this package.
	d, err := ma.dirPath()
	if err != nil {
		return &AuthError{"auth data not found", err}
	}

	err = os.RemoveAll(d)
	if err != nil {
		return err
	}

	err = setActiveUser("") // rms the active user file
	if err != nil {
		return err
	}

	if ma.AuthMode == IdentServer {
		ids.Logout()
	} else {
		oas.Logout()
	}

	// Logging out resets the auth mode.
	ma.AuthMode = OAuth

	return nil
}

// Refresh an existing token.
func (ma *MergeAPI) Refresh() error {

	refTk, err := ma.RefreshToken()
	if err != nil {
		return &AuthError{"read error", err}
	}

	tk := AuthToken{}
	am := ma.ConfiguredAuthMode()
	if am == IdentServer {
		tk, err = ids.Refresh(refTk)
	} else {
		tk, err = oas.Refresh(refTk)
	}
	if err != nil {
		return &AuthError{"Refresh", err}
	}

	if tk.AccessToken != "" {
		ma.writeFile(tk.AccessToken, "token")
	} else {
		return &AuthError{"Refresh Token is empty", nil}
	}

	return nil
}

func (ma *MergeAPI) LoggedIn() bool {

	_, err := ma.Token()
	if err != nil {
		return false
	}

	// TODO check the token validity.
	return true
}

// Token returns the auth token if it exists.
func (ma *MergeAPI) Token() (string, error) {
	return ma.readFile("token")
}

// RefreshToken returns the refresh token if it exists.
func (ma *MergeAPI) RefreshToken() (string, error) {
	return ma.readFile("reftoken")
}

// MergeUser returns the merge user id of the logged in user.
func (ma *MergeAPI) MergeUser() (string, error) {
	if ma.APIUser != "" {
		return ma.APIUser, nil
	}
	user, err := ma.readFile("id")
	ma.APIUser = user
	return user, err
}

func (ma *MergeAPI) App() (string, error) {
	if ma.AppName != "" {
		return ma.AppName, nil
	}
	name, err := ma.readFile("app")
	ma.AppName = name
	return name, err
}

func (ma *MergeAPI) API() (string, error) {
	if ma.URL != "" {
		return ma.URL, nil
	}
	api, err := ma.readFile("api")
	ma.URL = api
	return api, err
}

func (ma *MergeAPI) CertPath() (string, error) {
	if ma.Certpath != "" {
		return ma.Certpath, nil
	}
	path, err := ma.readFile("certpath")
	ma.Certpath = path
	return path, err
}

func (ma *MergeAPI) Certificate() (string, error) {
	path, err := ma.CertPath()
	if err != nil {
		return "", err
	}

	cert, err := ioutil.ReadFile(path)
	if err != nil {
		return "", &APIError{Msg: "failed to read cert", Err: err}
	}

	return string(cert), nil
}

func (ma *MergeAPI) ConfiguredAuthMode() AuthMode {
	value, err := ma.readFile("authmode")
	if err != nil {
		return OAuth
	}
	var am AuthMode
	return am.FromString(value)
}

func setActiveUser(username string) error {

	u, err := user.Current()
	if err != nil {
		return &AuthError{"failed to get current user", err}
	}

	err = ensureDir(fmt.Sprintf("%s/.config/merge", u.HomeDir), u)
	if err != nil {
		return err
	}

	if username != "" {
		f := fmt.Sprintf("%s/.config/merge/active-user", u.HomeDir)
		err = ioutil.WriteFile(f, []byte(username), 0600)
		if err != nil {
			return err
		}

		err = chownToUser(f)
		if err != nil {
			return err
		}

	} else {
		// remove the file.
		err = os.Remove(
			fmt.Sprintf("%s/.config/merge/active-user", u.HomeDir),
		)
		if err != nil {
			return err
		}
	}

	return nil
}

func ensureDir(path string, u *user.User) error {

	if _, err := os.Stat(path); os.IsNotExist(err) {
		err = os.MkdirAll(path, 0700)
		if err != nil {
			return &AuthError{"failed to ensure directory " + path, err}
		}

		err = chownToUser(path)
		if err != nil {
			return err
		}
	}
	return nil
}

func chownToUser(path string) error {

	u, err := user.Current()
	if err != nil {
		return &AuthError{"current user chownToUser", err}
	}

	uid, err := strconv.Atoi(u.Uid)
	if err != nil {
		return &AuthError{"Bad UID for current user", err}
	}

	gid, err := strconv.Atoi(u.Gid)
	if err != nil {
		return &AuthError{"Bad GID for current user", err}
	}

	// Force to be owned by current user/group.
	err = os.Chown(path, uid, gid)
	if err != nil {
		return &AuthError{"Failed to chown " + path, err}
	}

	return nil
}

func (ma *MergeAPI) dirPath() (string, error) {

	u, err := user.Current()
	if err != nil {
		return "", &AuthError{"failed to get current user", err}
	}

	err = ensureDir(fmt.Sprintf("%s/.config/merge", u.HomeDir), u)
	if err != nil {
		return "", err
	}

	if ma.APIUser == "" {
		path := fmt.Sprintf("%s/.config/merge/active-user", u.HomeDir)

		actuser, err := ioutil.ReadFile(path)
		if err != nil {
			return "", &AuthError{"No active api user set", nil}
		}
		ma.APIUser = string(actuser)
		if ma.APIUser == "" {
			return "", &AuthError{"active API user not set", nil}
		}
	}

	// Ex: ~glawler/.config/merge/tommy
	path := fmt.Sprintf("%s/.config/merge/%s", u.HomeDir, ma.APIUser)

	err = ensureDir(path, u)
	if err != nil {
		return "", err
	}

	return path, nil
}

func (ma *MergeAPI) writeFile(data, filename string) error {

	path, err := ma.dirPath()
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(
		path+"/"+filename,
		[]byte(data),
		0660,
	)
	if err != nil {
		return &AuthError{fmt.Sprintf("failed to write token to %s/%s", path, filename), err}
	}

	err = chownToUser(path + "/" + filename)
	if err != nil {
		return err
	}

	return nil
}

func (ma *MergeAPI) readFile(filename string) (string, error) {
	d, err := ma.dirPath()
	if err != nil {
		return "", err
	}

	path := fmt.Sprintf("%s/%s", d, filename)
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			// Not logged in?
			return "", &AuthError{"missing file", err}
		}
		return "", err
	}

	buf, err := ioutil.ReadFile(path)
	if err != nil {
		return "", &AuthError{"failed to read file", err}
	}

	return string(buf), nil
}

func (at AuthMode) String() string {
	switch at {
	case OAuth:
		return "OAuth"
	case IdentServer:
		return "IdentServer"
	}
	return "Unknown"
}

func (at AuthMode) FromString(s string) AuthMode {
	if s == "OAuth" {
		return OAuth
	} else if s == "IdentServer" {
		return IdentServer
	}
	return OAuth
}
