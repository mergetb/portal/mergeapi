package mergeapi

import (
	"gitlab.com/mergetb/engine/api/portal/models"
)

func (ma *MergeAPI) AllRealizations() ([]models.RzInfo, error) {

	var rlzs []models.RzInfo
	err := ma.GetObj("realizations", nil, &rlzs)
	if err != nil {
		return nil, err
	}

	return rlzs, nil
}

// Realizations lists the current realizations of the given experiment
func (ma *MergeAPI) Realizations(project, experiment string) ([]string, error) {

	var rlzs []string
	err := ma.GetObj("projects/"+project+"/experiments/"+experiment+"/realizations", nil, &rlzs)
	if err != nil {
		return nil, err
	}

	return rlzs, nil
}

// Realize creates a realization from a given experiment version.
func (ma *MergeAPI) Realize(project, experiment, realizeid, expverhash string) (*models.RealizeResponse, error) {

	resp := &models.RealizeResponse{}
	err := ma.PutObj(
		"projects/"+project+"/experiments/"+experiment+"/realizations/"+realizeid,
		&models.NewRealization{
			Name: realizeid,
			Hash: expverhash,
		},
		&resp,
	)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// Free frees the resources reserved for the given realization.
func (ma *MergeAPI) Free(project, experiment, realizeid string) error {

	return ma.Del("projects/"+project+"/experiments/"+experiment+"/realizations/"+realizeid, nil)
}

// Realization gets the information for the given realization.
func (ma *MergeAPI) Realization(project, experiment, realizeid string) (*models.Realization, error) {

	resp := &models.Realization{}
	err := ma.GetObj("projects/"+project+"/experiments/"+experiment+"/realizations/"+realizeid, nil, resp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// Accept accepts the given realization.
func (ma *MergeAPI) Accept(project, experiment, realizeid string) error {

	return ma.RealizeAction(project, experiment, realizeid, "accept")
}

// Reject rejects the given realization.
func (ma *MergeAPI) Reject(project, experiment, realizeid string) error {

	return ma.RealizeAction(project, experiment, realizeid, "reject")
}

// (The OpenAPI spec takes a string for "accept" or "reject". This function should take a strongly typed
// variable for the action, but it is not currently exposed as a package in the portal engine. TODO.)
func (ma *MergeAPI) RealizeAction(project, experiment, realizeid, action string) error {

	if action != "accept" && action != "reject" {
		return &APIError{Msg: "Realize action must be one of 'accept' or 'reject'"}
	}

	return ma.Post(
		"projects/"+project+"/experiments/"+experiment+"/realizations/"+realizeid+"/act",
		&models.RealizationAction{
			Action: action,
		},
	)
}
