package mergeapi

import (
	"bytes"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
)

//
// Encapsulates data and actions when using an Identity Server for authentication
//

// should be imported from gitlab.com/meergetb/portal/auth/merge-api-login
type LoginResult struct {
	Session string `json:"session"`
}

// should be imported from gitlab.com/meergetb/portal/auth/merge-api-login
type LoginRequest struct {
	Uid string `json:"uid"`
	Pw  string `json:"pw"`
}

type IDServer struct {
	Server string
}

func NewIdentityServer() *IDServer {
	return &IDServer{}
}

func (ids *IDServer) SetServer(srv string) {
	log.Debugf("IDServer set: %s", srv)
	ids.Server = srv
}

func (ids *IDServer) Login(uid, pw string, ma *MergeAPI) (AuthToken, error) {

	tk := AuthToken{}
	endpoint := "https://" + ids.Server + "/login"

	body, err := json.Marshal(LoginRequest{
		Uid: uid,
		Pw:  pw,
	})
	if err != nil {
		return tk, &AuthError{"body marshal", err}
	}

	client, err := ma.getHttpClient()
	if err != nil {
		return tk, &AuthError{"http client", err}
	}

	resp, err := client.Post(
		endpoint,
		"application/json",
		bytes.NewBuffer(body),
	)
	if err != nil {
		return tk, &AuthError{"/login POST", err}
	}
	defer resp.Body.Close()

	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return tk, &AuthError{"read resp body", err}
	}

	if resp.StatusCode != 200 {
		return tk, &AuthError{
			string(body),
			nil,
		}
	}

	var lr LoginResult
	err = json.Unmarshal(body, &lr)
	if err != nil {
		return tk, &AuthError{"unmarshal resp body", err}
	}

	return AuthToken{
		AccessToken:  lr.Session,
		RefreshToken: "yeahsure",
	}, nil

}

func (ids *IDServer) Refresh(refTk string) (AuthToken, error) {
	// Need to add a /refresh endpoint to merge-cli-login.
	return AuthToken{}, fmt.Errorf("not implemented")
}

func (ids *IDServer) Logout() {
	// noop for the moment,
	// We should contact the server and tell them the user has logged out
	// so it can clear any sessions it may have.
}
