package mergeapi

import (
	"io/ioutil"

	"gitlab.com/mergetb/engine/api/portal/models"
	xir "gitlab.com/mergetb/xir/lang/go/v0.2"
)

//
// Experiments ****************************************************************
//

// Experiments returns an array of experiment names for the given project.
func (ma *MergeAPI) Experiments(project string) ([]string, error) {

	exps := []string{}
	err := ma.GetObj("projects/"+project+"/experiments", nil, &exps)
	if err != nil {
		return nil, err
	}

	return exps, nil
}

// Experiment returns information on a specific experiment.
func (ma *MergeAPI) Experiment(project, experiment string) (*models.Experiment, error) {

	exp := &models.Experiment{}

	err := ma.GetObj("projects/"+project+"/experiments/"+experiment, nil, exp)
	if err != nil {
		return nil, err
	}

	return exp, nil
}

// NewExperiment creates a new experiment.
func (ma *MergeAPI) NewExperiment(project, experiment, description string) error {

	return ma.Put("projects/"+project+"/experiments/"+experiment, &models.Experiment{
		Name:        experiment,
		Project:     project,
		Description: description,
	})
}

// UpdateExperiment udpates an existing experiment.
func (ma *MergeAPI) UpdateExperiment(project, experiment, description string) error {
	return ma.Post(
		"projects/"+project+"/experiments/"+experiment,
		&models.Experiment{
			Name:        experiment,
			Project:     project,
			Description: description,
		},
	)
}

// DeleteExperiment deletes the given experiment.
func (ma *MergeAPI) DeleteExperiment(project, experiment string) error {
	return ma.Del("projects/"+project+"/experiments/"+experiment, nil)
}

// PushExperimentModel - takes a model string and pushes it to the given experiment.
func (ma *MergeAPI) PushExperimentModel(project, experiment, src string) (string, error) {

	xirObj, err := ma.CompileModel(src)
	if err != nil {
		return "", err
	}

	xirStr, err := xirObj.ToString()
	if err != nil {
		return "", &APIError{Msg: "compiled to bad xir", Err: err}
	}

	xpPush := &models.XpPush{
		Xir: xirStr,
		Src: string(src),
	}

	var hash string
	err = ma.PostObj("projects/"+project+"/experiments/"+experiment+"/src", xpPush, &hash)
	if err != nil {
		return "", err
	}

	return hash, nil
}

// PushExperimentModelFile pushes a new model onto the stack of experiment versions and
// and returns the experiment version hash.
func (ma *MergeAPI) PushExperimentModelFile(project, experiment, modelpath string) (string, error) {

	// We do a little extra work here as the API does not support pushing models natively.

	// This is a double read as we'll read it in CompileModel() as well. FIX?
	src, err := ioutil.ReadFile(modelpath)
	if err != nil {
		return "", &APIError{Msg: "bad path to model file", Err: err}
	}

	return ma.PushExperimentModel(project, experiment, string(src))
}

// PushExperimentXir pushes the xir onto the experiment version stack and returns
// the hash of the newly pushed version.
func (ma *MergeAPI) PushExperimentXir(project, experiment, xirpath string) (string, error) {

	net, err := xir.FromFile(xirpath)
	if err != nil {
		return "", &APIError{Msg: "bad path to xir file", Err: err}
	}

	xirStr, err := net.ToString()
	if err != nil {
		return "", &APIError{Msg: "Unable to parse xir", Err: err}
	}

	xpPush := &models.XpPush{
		Xir: xirStr,
		Src: "",
	}

	var hash string
	err = ma.PutObj("projects/"+project+"/experiments/"+experiment+"/src", xpPush, &hash)
	if err != nil {
		return "", err
	}

	return hash, nil

}

// PullExperimentModel gets the model for the given experiment version (hash)
func (ma *MergeAPI) PullExperimentModel(project, experiment, hash string) (*models.XpSource, error) {

	resp := &models.XpSource{}
	err := ma.GetObj("projects/"+project+"/experiments/"+experiment+"/src/"+hash, nil, resp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (ma *MergeAPI) History(pid, eid string) ([]string, error) {

	var hashes []string
	err := ma.GetObj("projects/"+pid+"/experiments/"+eid+"/src", nil, &hashes)
	if err != nil {
		return nil, err
	}

	return hashes, nil
}
