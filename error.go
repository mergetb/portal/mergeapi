package mergeapi

import (
	"errors"
	"fmt"
	"net/http"
)

// AuthError wraps the underlying authentication error in a custom error.
// It implements Error() and thus can be used as an error.
type AuthError struct {
	Msg string
	Err error
}

// APIError wraps API connection errors.
// It implements Error() and thus can be used as an error.
type APIError struct {
	Msg      string
	Err      error
	Response *http.Response
	Body     []byte
}

var (
	ErrNotLoggedin = errors.New("Not Logged In")
)

// Error implements the error interface for APIError
func (ae *APIError) Error() string {

	s := fmt.Sprintf("Msg: %s, Err: %s", ae.Msg, ae.Err.Error())

	if ae.Response != nil {
		s += fmt.Sprintf("Status: %d\n", ae.Response.StatusCode)
	}

	return s
}

func (ae *APIError) Unwrap() error {
	return ae.Err
}
