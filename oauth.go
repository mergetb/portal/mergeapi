package mergeapi

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type otoken struct {
	ClientID string `json:"client_id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Audience string `json:"audience"`
	Grant    string `json:"grant_type"`
	Realm    string `json:"realm"`
	Scope    string `json:"scope"`
	Device   string `json:"device"`
}

// AuthToken encapsulates an authorized token.
type AuthToken struct {
	AccessToken  string `json:"access_token"`
	IDToken      string `json:"id_token"`
	RefreshToken string `json:"refresh_token"`
	Expires      int    `json:"expires_in"`
	Type         string `json:"token_type"`
}

type OAServer struct {
	Audience string
	Realm    string
	Grant    string
	Scope    string
	ClientID string
	Endpoint string
}

func NewOAuthServer() *OAServer {
	return &OAServer{
		Audience: "https://mergetb.net/api/v1",
		Realm:    "Username-Password-Authentication",
		Grant:    "password",
		Scope:    "openid offline_access",
		ClientID: "0lZntyhSYdpGTtbfL0TfpwIZmb78IsGZ",
		Endpoint: "https://mergetb.auth0.com/oauth/token",
	}
}

func (oa *OAServer) SetAuthProvider(cliID, ep string) {
	oa.ClientID = cliID
	oa.Endpoint = "https://" + ep + "/oauth/token"
}

func (oa *OAServer) Login(uid, pw string) (AuthToken, error) {

	var authtk AuthToken

	payload, err := json.Marshal(otoken{
		ClientID: oa.ClientID,
		Username: uid,
		Password: pw,
		Audience: oa.Audience,
		Realm:    oa.Realm,
		Grant:    oa.Grant,
		Scope:    oa.Scope,
	})

	if err != nil {
		return authtk, &AuthError{"failed to serialize auth token", err}
	}

	resp, err := http.Post(oa.Endpoint, "application/json", bytes.NewReader(payload))
	if err != nil {
		return authtk, &AuthError{"failed to call auth endpoint", err}
	}
	if resp.StatusCode != 200 {
		return authtk, &AuthError{"authentication failed", nil}
	}

	out, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return authtk, &AuthError{"failed to read auth response", err}
	}

	err = json.Unmarshal(out, &authtk)
	if err != nil {
		return authtk, &AuthError{"failed to parse auth response", err}
	}

	return authtk, nil
}

func (oa *OAServer) Refresh(refTk string) (AuthToken, error) {

	tk := AuthToken{}

	if refTk == "" {
		return tk, &AuthError{"no refresh token", nil}
	}

	s := fmt.Sprintf(
		"grant_type=refresh_token&client_id=%s&refresh_token=%s",
		oa.ClientID, refTk,
	)

	payload := strings.NewReader(s)

	req, err := http.NewRequest(
		"POST",
		oa.Endpoint,
		payload,
	)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return tk, &AuthError{"token refresh request failed", nil}
	}
	defer resp.Body.Close()

	out, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return tk, &AuthError{"failed to read auth response: %v", err}
	}

	err = json.Unmarshal(out, &tk)
	if err != nil {
		return tk, &AuthError{"failed to parse auth refresh response: %v", err}
	}

	return tk, nil
}

func (oa *OAServer) Logout() {
	// noop for the moment,
	// We should contact the oauth server and tell them the user has logged out
	// so it can clear any sessions it may have.
}
