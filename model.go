package mergeapi

import (
	"io/ioutil"

	"gitlab.com/mergetb/engine/api/portal/models"
	xir "gitlab.com/mergetb/xir/lang/go/v0.2"
)

// CompileModelFile takes a path to a model file (mx) and returns the compiled Xir.
func (ma *MergeAPI) CompileModelFile(modelpath string) (*xir.Net, error) {

	src, err := ioutil.ReadFile(modelpath)
	if err != nil {
		return nil, &APIError{Msg: "bad path to model file", Err: err}
	}

	return ma.CompileModel(string(src))
}

type xirResp struct {
	XIR string `json:"xir"`
}

// CompileModel - compiles the given model as string to a xir.Net.
func (ma *MergeAPI) CompileModel(model string) (*xir.Net, error) {

	anonmodel := &models.AnonModel{
		Model: string(model),
		Type:  "mx",
	}

	// Something is off here. XIR.xir is an object in the openapi spec. The object is
	// not defined. yet we know it's a XIR. Assume it's a string and build from there. This is
	// wrong though...

	resp := &xirResp{}
	err := ma.PutObj("model/compile", anonmodel, resp)
	if err != nil {
		return nil, &APIError{Msg: "failed to compile model", Err: err}
	}

	// convert from string to Net
	net, err := xir.FromString(resp.XIR)
	if err != nil {
		return nil, &APIError{Msg: "model compiled to bad xir", Err: err}
	}

	return net, nil
}
