package mergeapi

import (
	"gitlab.com/mergetb/engine/api/portal/models"
)

// Pools get all pools
func (ma *MergeAPI) Pools() ([]models.Pool, error) {

	var pls []models.Pool
	err := ma.GetObj("pools", nil, &pls)
	if err != nil {
		return nil, err
	}
	return pls, nil
}

// CreatePool ...
func (ma *MergeAPI) CreatePool(pid, description string) error {

	return ma.Put("pools/"+pid, &models.Pool{Name: pid, Description: description})
}

// UpdatePool ...
func (ma *MergeAPI) UpdatePool(pid, description string) error {

	return ma.Post("pools/"+pid, &models.Pool{Name: pid, Description: description})
}

// Pool get info about a specific pool
func (ma *MergeAPI) Pool(pid string) (*models.Pool, error) {

	pl := &models.Pool{}
	err := ma.GetObj("pools/"+pid, nil, &pl)
	if err != nil {
		return nil, err
	}
	return pl, nil
}

func (ma *MergeAPI) DeletePool(pid string) error {

	return ma.Del("pools/"+pid, nil)
}

// PoolSites ....
func (ma *MergeAPI) PoolSites(pid string) ([]models.PoolSite, error) {

	var sites []models.PoolSite
	err := ma.GetObj("pools/"+pid+"/sites", nil, &sites)
	if err != nil {
		return nil, err
	}
	return sites, nil
}

// AddSite ...
func (ma *MergeAPI) AddPoolSite(poolid, site string) error {

	return ma.Put("pools/"+poolid+"/sites/"+site, nil)
}

func (ma *MergeAPI) AddPoolSiteView(poolid, site, view string) error {

	return ma.Put("pools/"+poolid+"/sites/"+site, &models.PoolSite{
		Name: site,
		View: view,
	})
}

// DeletePoolSite ...
func (ma *MergeAPI) DeletePoolSite(poolid, site string) error {

	return ma.Del("pools/"+poolid+"/sites/"+site, nil)
}

func (ma *MergeAPI) SetPoolSiteState(poolid, site, state string) error {

	return ma.Post("pools/"+poolid+"/sites/"+site, &models.PoolSiteState{State: state})
}

// PoolProjects ....
func (ma *MergeAPI) PoolProjects(poolid string) ([]models.PoolProject, error) {

	projects := []models.PoolProject{}
	err := ma.GetObj("pools/"+poolid+"/projects", nil, &projects)
	if err != nil {
		return nil, err
	}
	return projects, nil
}

// AddPoolProject ...
func (ma *MergeAPI) AddPoolProject(poolid, projectid string) error {

	return ma.Put("pools/"+poolid+"/projects/"+projectid, nil)
}

// DeletePoolProject ...
func (ma *MergeAPI) DeletePoolProject(poolid, projectid string) error {

	return ma.Del("pools/"+poolid+"/projects/"+projectid, nil)
}

// SetPoolProjectState ...
func (ma *MergeAPI) SetPoolProjectState(poolid, projectid, state string) error {

	return ma.Post(
		"pools/"+poolid+"/projects/"+projectid,
		&models.PoolProjectState{State: state},
	)
}

// PoolProjectState ...
func (ma *MergeAPI) PoolProjectState(poolid, projectid string) (string, error) {

	state := &models.PoolProjectState{}

	err := ma.GetObj(
		"pools/"+poolid+"/projects/"+projectid,
		nil,
		&state,
	)
	if err != nil {
		return "", err
	}
	return state.State, nil
}
