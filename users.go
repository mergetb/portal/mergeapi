package mergeapi

import (
	"gitlab.com/mergetb/engine/api/portal/models"
	"io/ioutil"
)

//
// Users **********************************************************************
//

// InitUser ...
func (ma *MergeAPI) InitUser() error {
	return ma.Get("user/init", nil)
}

// User -- get a user's information.
func (ma *MergeAPI) User(uid string) (*models.User, error) {

	user := &models.User{}
	err := ma.GetObj("users/"+uid, nil, &user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// PubKey ...
type PubKey struct {
	Key         string
	Fingerprint string
}

// PublicKeys returns public keys data for the given user.
func (ma *MergeAPI) PublicKeys(uid string) ([]PubKey, error) {

	keys := []models.PublicKey{}
	err := ma.GetObj("users/"+uid+"/keys", nil, &keys)
	if err != nil {
		return nil, err
	}

	ret := []PubKey{}
	for _, k := range keys {
		ret = append(ret, PubKey{k.Key, k.Fingerprint})
	}

	return ret, nil
}

// AddPublicKey ...
func (ma *MergeAPI) AddPublicKey(uid, key string) error {
	return ma.Put("users/"+uid+"/keys", &models.PublicKey{
		Key: key,
	})
}

func (ma *MergeAPI) AddPublicKeyFile(uid, path string) error {

	txt, err := ioutil.ReadFile(path)
	if err != nil {
		return &APIError{Msg: "failed to read file: " + path, Err: err}
	}

	return ma.Put("users/"+uid+"/keys", models.PublicKey{
		Key: string(txt),
	})
}

// DeletePublicKey ...
func (ma *MergeAPI) DeletePublicKey(uid, fingerprint string) error {
	return ma.Del("/users/"+uid+"/keys/"+fingerprint, nil)
}
