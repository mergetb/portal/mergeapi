package mergeapi

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/engine/pkg/merror"
)

//
// These are for when the endpoint returns nothing.
//

// Get ...
func (ma *MergeAPI) Get(path string, obj interface{}) error {
	_, err := ma.reqObj("GET", path, obj)
	return err
}

// Put ...
func (ma *MergeAPI) Put(path string, obj interface{}) error {
	_, err := ma.reqObj("PUT", path, obj)
	return err
}

// Post ..
func (ma *MergeAPI) Post(path string, obj interface{}) error {
	_, err := ma.reqObj("POST", path, obj)
	return err
}

// Del ...
func (ma *MergeAPI) Del(path string, obj interface{}) error {
	_, err := ma.reqObj("DELETE", path, obj)
	return err
}

//
// XObj functions are for when you give an object and expect one in return.
//

// GetObj ...
func (ma *MergeAPI) GetObj(path string, obj, out interface{}) error {

	return ma.reqObjRespObj("GET", path, obj, out)
}

// PutObj ...
func (ma *MergeAPI) PutObj(path string, obj, out interface{}) error {

	return ma.reqObjRespObj("PUT", path, obj, out)
}

// PostObj ...
func (ma *MergeAPI) PostObj(path string, obj, out interface{}) error {

	return ma.reqObjRespObj("POST", path, obj, out)
}

// DelObj ...
func (ma *MergeAPI) DelObj(path string, obj, out interface{}) error {
	return ma.reqObjRespObj("DELETE", path, obj, out)
}

//
// When you just want the raw return data.
//

// GetRaw ...
func (ma *MergeAPI) GetRaw(path string) ([]byte, error) {
	return ma.reqRaw("GET", path, nil)
}

// PutRaw ...
func (ma *MergeAPI) PutRaw(path string, buf []byte) ([]byte, error) {
	return ma.reqRaw("PUT", path, buf)
}

// DelRaw ...
func (ma *MergeAPI) DelRaw(path string, buf []byte) ([]byte, error) {
	return ma.reqRaw("DELETE", path, buf)
}

// PostRaw ...
func (ma *MergeAPI) PostRaw(path string, buf []byte) ([]byte, error) {
	return ma.reqRaw("POST", path, buf)
}

// unexported functions

func (ma *MergeAPI) reqRaw(method, path string, buf []byte) ([]byte, error) {

	return ma.apiRequest(method, path, bytes.NewReader(buf))

}

func (ma *MergeAPI) reqObj(method, path string, obj interface{}) ([]byte, error) {

	buf, err := json.MarshalIndent(obj, "", "  ")
	if err != nil {
		return nil, &APIError{Msg: "failed to marshal object", Err: err}
	}

	return ma.reqRaw(method, path, buf)
}

func (ma *MergeAPI) reqObjRespObj(method, path string, obj, out interface{}) error {
	buf, err := ma.reqObj(method, path, obj)
	if err != nil {
		return err
	}

	err = json.Unmarshal(buf, out)
	if err != nil {
		return &APIError{Msg: "failed to unmarshal object", Err: err}
	}

	return nil
}

func (ma *MergeAPI) apiRequest(method, path string, body io.Reader) ([]byte, error) {

	// Default action for request is to try to refresh the token if expired.
	return ma.apiRequestRefresh(method, path, body, 3)

}

func (ma *MergeAPI) apiRequestRefresh(method, path string, body io.Reader, refreshCnt int) ([]byte, error) {

	token, err := ma.Token()
	if err != nil {
		return nil, &AuthError{"Auth token not found", ErrNotLoggedin}
	}

	api, err := ma.API()
	if err != nil {
		return nil, &APIError{Msg: "no api", Err: err}
	}

	authmode := ma.ConfiguredAuthMode()

	req, err := http.NewRequest(
		method,
		"https://"+api+"/"+path,
		body,
	)
	if err != nil {
		return nil, &APIError{Msg: "new request failed", Err: err}
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	req.Header.Add("MergeAPIAuthMode", authmode.String())

	client, err := ma.getHttpClient()
	if err != nil {
		return nil, &APIError{Msg: "http client", Err: err}
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, &APIError{Msg: "request failed", Err: err}
	}

	//on auth failure, attempt to refresh the token and login again.
	if resp.StatusCode == 401 {
		if refreshCnt == 0 {
			return nil, &APIError{"Unauthorized. Token refresh failure.", err, resp, nil}
		}

		err := ma.Refresh() // Use the refresh token to get a new access token then try again.
		if err != nil {
			log.Warnf(
				"Unable to refresh auth token. Trying %d more times in 1 second.",
				refreshCnt,
			)
			time.Sleep(time.Second * 1)
		}

		return ma.apiRequestRefresh(method, path, body, refreshCnt-1)
	}

	if resp.StatusCode != 200 {
		b, _ := ioutil.ReadAll(resp.Body)
		aErr := &APIError{
			Msg:      "API Error",
			Response: resp,
			Body:     b,
		}
		me, err := merror.FromModelError(b)
		if err == nil {
			aErr.Err = me
		}

		return nil, aErr
	}

	out, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, &APIError{Msg: "failed to read portal response", Err: err}
	}

	log.Debugf("resp body: %s", string(out))

	return out, nil
}

func (ma *MergeAPI) getHttpClient() (*http.Client, error) {

	var client *http.Client
	cert, _ := ma.Certificate()
	to := time.Second * 0 // zero == never timeout.

	if cert != "" {

		pool, _ := x509.SystemCertPool()
		if pool == nil {
			pool = x509.NewCertPool()
		}

		ok := pool.AppendCertsFromPEM([]byte(cert))
		if !ok {
			return nil, &APIError{Msg: "failed to add cert to pool", Err: nil}
		}

		client = &http.Client{
			Timeout: to,
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					RootCAs: pool,
				},
			},
		}

	} else {
		client = &http.Client{
			Timeout: to,
		}
	}

	return client, nil
}
