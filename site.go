package mergeapi

import (
	"bufio"
	"gitlab.com/mergetb/engine/api/portal/models"
	"os"
)

// Sites ...
func (ma *MergeAPI) Sites() ([]models.Site, error) {

	sites := []models.Site{}
	err := ma.GetObj("sites", nil, &sites)
	if err != nil {
		return nil, err
	}

	return sites, nil
}

// CreateSite ...
func (ma *MergeAPI) CreateSite(site, address, model, cert string) (*models.RegisterResponse, error) {

	// rr := &models.RegisterResponse{}
	// See https://gitlab.com/mergetb/portal/services/-/blob/master/client/commission.go#L62
	// This api endpoint should return something, but it does not.
	err := ma.Put(
		"sites/"+site,
		&models.NewSite{
			Name:    site,
			Address: address,
			Model:   model,
		},
		// &rr, // see comment above
	)
	if err != nil {
		return nil, err
	}
	// return rr, nil
	return nil, nil // see comment about.
}

// Site get information about a site.
func (ma *MergeAPI) Site(sitename string) (*models.Site, error) {

	site := &models.Site{}
	err := ma.GetObj("sites/"+sitename, nil, site)
	if err != nil {
		return nil, err
	}

	return site, nil
}

// UpdateSite update an existing site.
func (ma *MergeAPI) UpdateSite(site, address, model, cert string) error {

	s := &models.Site{
		Name: site,
	}

	if address != "" {
		s.Address = address
	}

	if model != "" {
		s.Model = model
	}

	if cert != "" {
		s.Cert = cert
	}

	return ma.Post("sites/"+site, s)
}

func (ma *MergeAPI) SiteCert(site, cert string) error {

	return ma.Post("sites/"+site+"/cert", &models.SiteCert{
		Cert: cert,
	})
}

// DeleteSite ...
func (ma *MergeAPI) DeleteSite(site string) error {

	// This does return a RegisterResponse, but I think it's a cut and
	// paste error in the open api specification.
	return ma.Del("sites/"+site, nil)
}

// SetWireguardConfig ...
func (ma *MergeAPI) SetWireguardConfig(site, address, cert string) error {
	return ma.Post("sites/"+site+"/wgdconfig", &models.WgdSiteConfig{
		Address: address,
		Cert:    cert,
	})
}

// Activate - activate a existing site.
func (ma *MergeAPI) Activate(sitename string, resources []string) error {

	return ma.Post("sites/"+sitename+"/activate", resources)
}

// Deactivate - deactivate existing site resources.
func (ma *MergeAPI) Deactivate(sitename string, resources []string) error {

	return ma.Post("sites/"+sitename+"/deactivate", resources)
}

// Resources - lists all available resources across all sites?
func (ma *MergeAPI) Resources() ([]models.Resource, error) {

	r := []models.Resource{}
	err := ma.GetObj("/resources", nil, &r)
	if err != nil {
		return nil, err
	}

	return r, nil
}

func (ma *MergeAPI) CreateSiteViewFile(site, view, path string, isDefault bool) error {

	fd, err := os.Open(path)
	if err != nil {
		return err
	}
	defer fd.Close()

	nodes := []string{}
	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		nodes = append(nodes, scanner.Text())
	}

	return ma.CreateSiteView(site, view, nodes, isDefault)
}

func (ma *MergeAPI) CreateSiteView(site, view string, nodes []string, isDefault bool) error {

	return ma.Put("sites/"+site+"/views/"+view, &models.SiteView{
		Name:      view,
		Nodes:     nodes,
		Isdefault: isDefault,
	})
}

func (ma *MergeAPI) DeleteSiteView(site, view string) error {

	return ma.Del("sites/"+site+"/views/"+view, nil)
}

func (ma *MergeAPI) SiteView(site, view string) (*models.SiteView, error) {

	v := &models.SiteView{}
	err := ma.GetObj("sites/"+site+"/views/"+view, nil, &v)
	if err != nil {
		return nil, err
	}

	return v, nil
}

func (ma *MergeAPI) SiteViews(site string) ([]string, error) {
	var views []string
	err := ma.GetObj("sites/"+site+"/views", nil, &views)
	if err != nil {
		return nil, err
	}

	return views, nil
}

func (ma *MergeAPI) DefaultView(site, view string) error {

	return ma.Put("sites/"+site+"/views/"+view+"/default", nil)
}
