package mergeapi

//
// Test suite for mergeapi.
// ---------------------------------------------------------------------
//
// This currently assumes
// * There is a VTE up and running with spineleaf site active
// * The variables are in the env: MERGEUSER, MERGEPW, MERGEMEMBER
// * Accounts pointed to by env vars are valid and active in the VTE
// * No public keys for any user
//
import (
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/mergetb/engine/pkg/merror"
	"io/ioutil"
	"os"
	"os/user"
	"strings"
	"testing"
	"time"
)

var (
	pid      = "testproject"
	projdesc = "this is a description"
	projmode = "public"
	eid      = "blarg"
	expdesc  = "this is an experiment"
	rid      = "testrlz"
)

var (
	key = `
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDlUgFjXQrFSwfiRJDQjCVAxmAK5B6PbqNl2+29MXsiR3CuPZfTbIbZlsvAt9kPPj2FgKB9Z0GufviIk82Jr6DZo4DypoEfb494UsmCCs+iKa/0Hr82U3akGGe8gqsY6z4pqmQUkHxkMJDP7yYssMcrCZP58/VMhr2B9UbfKj042aCmKoQAG6eDVHcD/BhAitxDsmESj1iJCsRMNzVcEkAD9rgIAuhlMB0Wvp8St40k7nV4fmZSmsacz9IutLljtsAUPr0tRTbZqbLP1OfHkjoJ14NuZYJqRd2X0iqUQ7Q1nFfEThFT/Z4UZJL45faT/Ua9gRXCTI0a5c21vzZ0q6OM7+5K1GG3s6T4Mc4lAqP6q3NgvQegkE6VldPj1WVD/ra3GaI+WAgI3uTR/Q0dzQ/yKPlT8M2OsHgHnjGEsamfsczT72T7iceDVZW9u+FU0zV/4dfF2QBE5ClqMqNpmc+qG3rFg/n8Rviqjw9eIbhIgbk1zxMc6E25/RQ6JdI86AKELgA2ZP9eFOVtvtBlEM5eZRxt27qJi+wbQe35M+IJEcuqRuECBzklZfTrJ5Xyy9fg5Uh34Zn6qJy84X7HbdIQeMtkzRDoM5mUNsGI06PMwCgyPIXSmbI/AAyP1k5mRgbPDZeBqLYXN8tQCLNewsolpBbA3/fxL2RsF5MSxKG1jw== murphy@isi.edu
`
	model = `
import mergexp as mx
net = mx.Topology('HelloThere')
nodes = [net.device(n) for n in ['a', 'b', 'c']]
net.connect(nodes)
mx.experiment(net)
`
	badModel = `
import mergexp as mx
from mergexp.machine import memory
from mergexp.unit import gb

net = mx.Topology('tooMuchMem')
a = net.device('a')
b = net.device('b', memory >= gb(10000))
net.connect([a,b])
mx.experiment(net)
`
)

func init() {
	// log.SetLevel(log.DebugLevel)
	log.SetLevel(log.InfoLevel)
}

func getPasswd(t *testing.T) string {
	pw := os.Getenv("MERGEPW")
	if pw == "" {
		t.Fatal("no passwd set in $MERGEPW")
	}
	return pw
}

func getUser(t *testing.T) string {
	u := os.Getenv("MERGEUSER")
	if u == "" {
		t.Fatal("no user set in $MERGEUSER")
	}
	return u
}

func getMember(t *testing.T) string {
	u := os.Getenv("MERGEMEMBER")
	if u == "" {
		t.Fatal("no user set in MERGEMEMBER")
	}
	return u
}

func api(t *testing.T) *MergeAPI {
	return &MergeAPI{
		AppName:  "testapi",
		APIUser:  getUser(t),
		URL:      "api.mergetb.net",
		Certpath: "/tmp/portal-keygen/ca.pem",
	}
}

func withLogin(t *testing.T) {
	api(t).Login(getPasswd(t))
}

func withoutLogin(t *testing.T) {
	api(t).Logout()
}

func withProject(t *testing.T) {
	withLogin(t)
	api(t).NewProject(pid, projdesc, projmode)
}

func withoutProject(t *testing.T) {
	api(t).DeleteProject(pid)
	withoutLogin(t)
}

func withExperiment(t *testing.T) {
	withProject(t)
	api(t).NewExperiment(pid, eid, expdesc)
}

func withoutExperiment(t *testing.T) {
	api(t).DeleteExperiment(pid, eid)
	withoutProject(t)
}

func withExperimentHash(t *testing.T) string {
	withExperiment(t)
	hsh, _ := api(t).PushExperimentModel(pid, eid, model)
	return hsh
}

func withRealization(t *testing.T) {
	hsh := withExperimentHash(t)
	api(t).Realize(pid, eid, rid, hsh)
	api(t).Accept(pid, eid, rid)
}

func withoutRealization(t *testing.T) {
	api(t).Free(pid, eid, rid)
	withoutExperiment(t)
}

func confirmLogin(merge *MergeAPI, assert *assert.Assertions) {
	u, _ := user.Current()
	uid, _ := merge.MergeUser()
	assert.FileExists(fmt.Sprintf("%s/.config/merge/%s/token", u.HomeDir, uid))
	assert.FileExists(fmt.Sprintf("%s/.config/merge/%s/id", u.HomeDir, uid))
	assert.FileExists(fmt.Sprintf("%s/.config/merge/%s/reftoken", u.HomeDir, uid))
	assert.FileExists(fmt.Sprintf("%s/.config/merge/%s/app", u.HomeDir, uid))
	assert.FileExists(fmt.Sprintf("%s/.config/merge/%s/api", u.HomeDir, uid))
	assert.FileExists(fmt.Sprintf("%s/.config/merge/%s/certpath", u.HomeDir, uid))

	assert.True(merge.LoggedIn())
}

func TestLogin(t *testing.T) {
	assert := assert.New(t)
	merge := api(t)

	err := merge.Login(getPasswd(t))
	defer withoutLogin(t)
	assert.Nil(err)

	confirmLogin(merge, assert)
}

func TestBadLogin(t *testing.T) {
	assert := assert.New(t)
	merge := api(t)

	err := merge.Login("blargblarg!")

	// merge api auth functions all return AuthErrors
	re, ok := err.(*AuthError)
	assert.Equal(ok, true)
	assert.Equal(re.Msg, "authentication failed")
}

func TestIDLogin(t *testing.T) {
	assert := assert.New(t)
	merge := api(t)

	merge.SetIdentityServer("id.mergetb.net")

	err := merge.Login(getPasswd(t))
	assert.Nil(err)
	defer withoutLogin(t)

	confirmLogin(merge, assert)
}

func TestLogout(t *testing.T) {
	withLogin(t)

	assert := assert.New(t)
	merge := api(t)

	assert.True(merge.LoggedIn())

	merge.Logout()

	assert.False(merge.LoggedIn())

	u, _ := user.Current()
	uid, _ := merge.MergeUser()
	assert.NoFileExists(fmt.Sprintf("%s/.config/merge/%s/token", u.HomeDir, uid))
}

func TestUserCrud(t *testing.T) {
	withLogin(t)

	assert := assert.New(t)
	merge := api(t)
	uid := merge.APIUser

	err := merge.InitUser()
	if !assert.Nil(err) {
		t.Fail()
	}

	u, err := merge.User(uid)
	if !assert.Nil(err) {
		t.Fail()
	}
	assert.Equal(u.AccessMode, "public")
	assert.Equal(u.State, "active")
	assert.Equal(u.Username, strings.ToLower(uid))

	// TODO: add tests for creatings users with unusal
	// chars in their usernames. (This requires ability
	// to activate accounts from here somehow...)

	err = merge.AddPublicKey(uid, key)
	if !assert.Nil(err) {
		t.Fail()
	}

	keys, err := merge.PublicKeys(uid)
	if !assert.Nil(err) {
		t.Fail()
	}
	if assert.Equal(1, len(keys)) {
		assert.Equal(keys[0].Key, key)
	}

	err = merge.DeletePublicKey(uid, keys[0].Fingerprint)
	if !assert.Nil(err) {
		t.Fail()
	}

	keys, err = merge.PublicKeys(uid)
	if !assert.Nil(err) {
		t.Fail()
	}
	assert.Equal(0, len(keys))
}

func TestReadMergeUser(t *testing.T) {
	withLogin(t)
	defer withoutLogin(t)

	assert := assert.New(t)
	merge := api(t)

	u, err := merge.MergeUser()
	if !assert.Nil(err) {
		t.Fail()
	}
	assert.Equal(getUser(t), u)

	merge.APIUser = ""

	u, err = merge.MergeUser()
	if !assert.Nil(err) {
		t.Fail()
	}
	assert.Equal(getUser(t), u)
}

func TestRefresh(t *testing.T) {
	withLogin(t)
	defer withoutLogin(t)

	assert := assert.New(t)
	api := api(t)

	tkn, err := api.Token()
	assert.Nil(err)

	err = api.Refresh()
	assert.Nil(err)

	newTkn, err := api.Token()
	assert.Nil(err)

	assert.NotEqual(tkn, newTkn)
}

func TestCrudProject(t *testing.T) {

	withLogin(t)
	defer withoutLogin(t)

	assert := assert.New(t)
	merge := api(t)

	err := merge.NewProject(pid, projdesc, projmode)
	assert.Nil(err)

	proj, err := merge.Project(pid)
	assert.Nil(err)
	assert.Equal(pid, proj.Name)
	assert.Equal(projdesc, proj.Description)
	assert.Equal("public", proj.AccessMode)

	// update then re-get the project to confirm updates.
	newdesc := "a new desc"
	newmode := "private"
	err = merge.UpdateProject(pid, newdesc, newmode)
	assert.Nil(err)

	proj, _ = merge.Project(pid)
	assert.Equal(pid, proj.Name)
	assert.Equal(newdesc, proj.Description)
	assert.Equal(newmode, proj.AccessMode)

	err = merge.DeleteProject(pid)
	assert.Nil(err)
}

func TestCrudExperiment(t *testing.T) {

	withProject(t)
	defer withoutProject(t)

	assert := assert.New(t)
	merge := api(t)

	err := merge.NewExperiment(pid, eid, expdesc)
	if !assert.Nil(err) {
		t.Fail()
	}

	exp, err := merge.Experiment(pid, eid)
	if !assert.Nil(err) {
		t.Fail()
	}

	assert.Equal(pid, exp.Project)
	assert.Equal(eid, exp.Name)
	assert.Equal(expdesc, exp.Description)

	// Update
	newdesc := "a new desc"
	err = merge.UpdateExperiment(pid, eid, newdesc)
	if !assert.Nil(err) {
		t.Fail()
	}

	exp, err = merge.Experiment(pid, eid)
	if !assert.Nil(err) {
		t.Fail()
	}

	assert.Equal(pid, exp.Project)
	assert.Equal(eid, exp.Name)
	assert.Equal(newdesc, exp.Description)

	// exp names & exp listing.
	anotherExp := "AnOtHerExp"
	err = merge.NewExperiment(pid, anotherExp, "desc")
	if !assert.Nil(err) {
		t.Fail()
	}

	exps, err := merge.Experiments(pid)
	if !assert.Nil(err) {
		t.Fail()
	}

	assert.Contains(exps, eid)
	assert.NotContains(exps, anotherExp)
	assert.Contains(exps, strings.ToLower(anotherExp))

	err = merge.DeleteExperiment(pid, eid)
	if !assert.Nil(err) {
		t.Fail()
	}
}

func TestIDLoginListProjects(t *testing.T) {

	t.Skipf("Skip until the Merge API understands Identity Servers")

	assert := assert.New(t)
	merge := api(t)

	merge.SetIdentityServer("id.mergetb.net")
	withProject(t)

	projs, err := merge.Projects()
	if !assert.Nil(err) {
		t.Fail()
	}

	assert.Equal(len(projs), 1)
}

func TestExperimentHistory(t *testing.T) {
	withExperiment(t)
	defer withoutExperiment(t)

	assert := assert.New(t)
	merge := api(t)
	user := merge.APIUser

	one, err := merge.PushExperimentModel(pid, eid, model)
	if !assert.Nil(err) {
		t.Fail()
	}

	oneSrc, err := merge.PullExperimentModel(pid, eid, one)
	if !assert.Nil(err) {
		t.Fail()
	}

	assert.Equal(model, oneSrc.Src)
	assert.Equal(user, oneSrc.Who)

	tmp, err := ioutil.TempFile(os.TempDir(), "mapitest-")
	if !assert.Nil(err) {
		t.Fail()
	}
	defer os.Remove(tmp.Name())

	_, err = tmp.Write([]byte(model))
	if !assert.Nil(err) {
		t.Fail()
	}

	// TODO: Apparently the experiment versions are *not* guarenteed to be in push order
	// if they are pushed within the same second. So sleep for a bit.
	time.Sleep(2 * time.Second)

	two, err := merge.PushExperimentModelFile(pid, eid, tmp.Name())
	if !assert.Nil(err) {
		t.Fail()
	}

	twoSrc, err := merge.PullExperimentModel(pid, eid, two)
	if !assert.Nil(err) {
		t.Fail()
	}

	assert.Equal(model, twoSrc.Src)
	assert.Equal(user, twoSrc.Who)

	// TODO test merge.PushExperimentXir() as three

	hashes, err := merge.History(pid, eid)
	if !assert.Nil(err) {
		t.Fail()
	}

	// hashes are in order pushed
	assert.Equal(2, len(hashes))
	assert.Equal(hashes[0], one)
	assert.Equal(hashes[1], two)
}

func TestModelCompile(t *testing.T) {
	withExperiment(t)
	defer withoutExperiment(t)

	assert := assert.New(t)
	merge := api(t)
	src := `
import mergexp as mx

net = mx.Topology('twonode')
(a,b) = [net.device(name) for name in ['a', 'b']]
net.connect([a,b])
mx.experiment(net)
`
	net, err := merge.CompileModel(src)
	if !assert.Nil(err) {
		t.Fail()
	}
	assert.NotNil(net)

	tmp, err := ioutil.TempFile(os.TempDir(), "mapitest-")
	if !assert.Nil(err) {
		t.Fail()
	}
	defer os.Remove(tmp.Name())

	_, err = tmp.Write([]byte(src))
	if !assert.Nil(err) {
		t.Fail()
	}

	net, err = merge.CompileModelFile(tmp.Name())
	if !assert.Nil(err) {
		t.Fail()
	}
	assert.NotNil(net)
}

func TestRealize(t *testing.T) {

	hash := withExperimentHash(t)
	defer withoutExperiment(t)

	assert := assert.New(t)
	merge := api(t)

	resp, err := merge.Realize(pid, eid, rid, hash)
	assert.Nil(err)
	assert.NotEqual(resp, nil)

	err = merge.Accept(pid, eid, rid)
	assert.Nil(err)

	// make sure it's there.
	reals, err := merge.Realizations(pid, eid)
	assert.Nil(err)
	assert.Contains(reals, rid)

	// The withoutExperiment() will delete the exp, freeing the realization,
	// but we do it here anyway for testing.
	err = merge.Free(pid, eid, rid)
	assert.Nil(err)

	reals, err = merge.Realizations(pid, eid)
	assert.Nil(err)
	assert.NotContains(reals, rid)
}

func TestBadRealize(t *testing.T) {
	// Load a model that will not realize
	withExperiment(t)
	defer withoutExperiment(t)

	assert := assert.New(t)
	merge := api(t)

	hash, _ := merge.PushExperimentModel(pid, eid, badModel)

	_, err := merge.Realize(pid, eid, rid, hash)
	assert.NotNil(err)

	// Assert it's an actual fail to realize error.
	var me *merror.MergeError
	if errors.As(err, &me) {
		// t.Logf("merge error %s", me)
		assert.True(errors.Is(me, merror.ErrFailedToRealize))

	} else {
		t.Fatal("did not get merge error")
	}
}

func TestListRealizations(t *testing.T) {
	withRealization(t)
	defer withoutRealization(t)

	assert := assert.New(t)
	merge := api(t)

	rlzs, err := merge.AllRealizations()
	assert.Nil(err)
	if assert.Equal(1, len(rlzs)) {
		assert.Equal(3, int(rlzs[0].Nodes))
	}
}

func TestGetRealization(t *testing.T) {
	withRealization(t)
	defer withoutRealization(t)

	assert := assert.New(t)
	merge := api(t)

	rlz, err := merge.Realization(pid, eid, rid)
	// t.Logf("rlz: %+v", rlz)

	assert.Nil(err)
	assert.Equal(true, rlz.Complete)
	assert.NotNil(rlz.Nodes)
	assert.NotNil(rlz.Links)
	assert.NotNil(rlz.Creator)
	assert.NotNil(rlz.Xhash)
	assert.NotNil(rlz.Zid)
}

func TestRealizeReject(t *testing.T) {
	hash := withExperimentHash(t)
	defer withoutExperiment(t)

	assert := assert.New(t)
	merge := api(t)

	resp, err := merge.Realize(pid, eid, rid, hash)
	assert.Nil(err)
	assert.NotEqual(resp, nil)

	err = merge.Reject(pid, eid, rid)
	assert.Nil(err)

	// make sure it's not there.
	reals, err := merge.Realizations(pid, eid)
	assert.Nil(err)

	found := false
	for _, real := range reals {
		if real == rid {
			found = true
			break
		}
	}
	assert.Equal(found, false)
}

func TestProjectMemberCrud(t *testing.T) {

	withProject(t)
	defer withoutProject(t)

	// confirm there are members
	assert := assert.New(t)
	merge := api(t)
	user := getUser(t)
	member := getMember(t)

	// only creator is a member
	members, err := merge.ProjectMembers(pid)
	if !assert.Nil(err) {
		t.Fail()
	}
	assert.Contains(members, user)
	assert.Equal(1, len(members))

	// add new member and confirm they are there
	err = merge.AddProjectMember(pid, member, "member")
	assert.Nil(err)

	members, err = merge.ProjectMembers(pid)
	if !assert.Nil(err) {
		t.Fail()
	}

	assert.Equal(2, len(members))
	assert.Contains(members, user)
	assert.Contains(members, member)

	memberInfo, err := merge.ProjectMember(pid, member)
	if !assert.Nil(err) {
		t.Fail()
	}
	assert.Equal(memberInfo.Member, member)
	assert.Equal(memberInfo.Role, "member")
	assert.Equal(memberInfo.State, "active")

	// delete the member
	err = merge.DeleteProjectMember(pid, member)
	if !assert.Nil(err) {
		t.Fail()
	}

	// only creator is a member
	members, err = merge.ProjectMembers(pid)
	if !assert.Nil(err) {
		t.Fail()
	}
	assert.Contains(members, user)
	assert.Equal(1, len(members))
}

func TestXDC(t *testing.T) {
	withExperiment(t)
	defer withoutExperiment(t)

	assert := assert.New(t)
	merge := api(t)
	xid := "myxdc"

	err := merge.Spawn(pid, eid, xid)
	if !assert.Nil(err) {
		t.Fail()
	}

	xdcs, err := merge.XDCs(pid, eid)
	if !assert.Nil(err) {
		merge.DeleteXDC(pid, eid, xid)
		t.Fail()
	}

	if !assert.Equal(1, len(xdcs)) {
		merge.DeleteXDC(pid, eid, xid)
		t.Fail()
	}

	// we use published docs to build fqdn format
	// (that may be a lie?)
	fqdn := xid + "-" + eid + "-" + pid + ".xdc.mergetb.io"
	assert.Equal(fqdn, xdcs[0].Fqdn)
	assert.Equal(xid, xdcs[0].Name)

	// just confirm we can get a token. verification is out of scope
	tkn, err := merge.XDCToken(pid, eid, xid)
	if !assert.Nil(err) {
		t.Fail()
	}

	assert.NotNil(tkn)

	err = merge.DeleteXDC(pid, eid, xid)
	if !assert.Nil(err) {
		t.Fail()
	}

	xdcs, err = merge.XDCs(pid, eid)
	if !assert.Nil(err) {
		t.Fail()
	}

	assert.Equal(0, len(xdcs))
}
