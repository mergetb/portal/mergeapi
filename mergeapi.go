package mergeapi

type AuthMode int

const (
	OAuth AuthMode = iota
	IdentServer
)

// MergeAPI encapsulates data about an authenticated REST API to a merge api server
// and the calls into the API itself.
type MergeAPI struct {
	AppName  string // application using the API
	APIUser  string // User ID for the API
	Certpath string // path to cert pem if not standard
	URL      string // base URL of REST API server

	AuthMode AuthMode // Which authentication mechanism we are using.
}

// See the other *.go files for the MergeAPI "interface".
