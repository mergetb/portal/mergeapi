module gitlab.com/mergetb/mergeapi

go 1.13

require (
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/k0kubun/pp v3.0.1+incompatible // indirect
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/testify v1.6.1
	gitlab.com/mergetb/engine v0.5.19
	gitlab.com/mergetb/xir v0.2.12
)
