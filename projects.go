package mergeapi

import (
	"gitlab.com/mergetb/engine/api/portal/models"
)

//
// Projects *******************************************************************
//

// Projects returns a list of projects the user can access.
func (ma *MergeAPI) Projects() ([]models.Project, error) {

	var projects []models.Project

	err := ma.GetObj("projects", nil, &projects)
	if err != nil {
		return nil, err
	}

	return projects, nil
}

// Project returns information about the project.
func (ma *MergeAPI) Project(project string) (*models.Project, error) {

	p := &models.Project{}
	err := ma.GetObj("/projects/"+project, nil, &p)
	if err != nil {
		return nil, err
	}

	return p, err
}

// NewProject creates a new project with the given data.
func (ma *MergeAPI) NewProject(name, description, mode string) error {
	return ma.Put(
		"projects/"+name,
		&models.Project{
			Name:        name,
			Description: description,
			AccessMode:  mode,
		},
	)
}

// UpdateProject updates an existing project.
func (ma *MergeAPI) UpdateProject(name, description, mode string) error {
	return ma.Post(
		"projects/"+name,
		&models.Project{
			Name:        name,
			Description: description,
			AccessMode:  mode,
		},
	)
}

// DeleteProject deletes the given project
func (ma *MergeAPI) DeleteProject(name string) error {
	return ma.Del("projects/"+name, nil)
}

// ProjectMembers returns an array of member names
func (ma *MergeAPI) ProjectMembers(project string) ([]string, error) {

	var ms []string
	err := ma.GetObj("projects/"+project+"/members", nil, &ms)
	if err != nil {
		return nil, err
	}

	return ms, nil
}

// AddProjectMember adds a user to a project as an active member.
func (ma *MergeAPI) AddProjectMember(project, member, role string) error {

	return ma.Put(
		"projects/"+project+"/members/"+member,
		&models.ProjectMember{
			Member:  member,
			Project: project,
			Role:    role,
			State:   "active",
		},
	)
}

// UpdateProjectMember updates an existing member.
func (ma *MergeAPI) UpdateProjectMember(project, member, role, state string) error {

	return ma.Post(
		"projects/"+project+"/members/"+member,
		&models.ProjectMember{
			Member:  member,
			Project: project,
			Role:    role,
			State:   state,
		},
	)
}

// ProjectMember returns a member's information.
func (ma *MergeAPI) ProjectMember(project, member string) (*models.ProjectMember, error) {

	obj := &models.ProjectMember{}
	err := ma.GetObj("projects/"+project+"/members/"+member, nil, &obj)
	if err != nil {
		return nil, err
	}

	return obj, nil
}

// DeleteProjectMember deletes the user from the project.
func (ma *MergeAPI) DeleteProjectMember(project, member string) error {
	return ma.Del("projects/"+project+"/members/"+member, nil)
}
